package main

import "testing"

func TestStringToBinaryNum(t *testing.T) {

	var testTable = []struct {
		line     string
		zeroChar rune
		oneChar  rune
		expected int
	}{
		{"BFFFBBF", 'F', 'B', 70},
		{"RRR", 'L', 'R', 7},
		{"FFFBBBF", 'F', 'B', 14},
		{"BBFFBBF", 'F', 'B', 102},
		{"RLL", 'L', 'R', 4},
		{"RLL ", 'L', 'R', 4},
		{" RLL", 'L', 'R', 4},
		{" RL L", 'L', 'R', 4},
	}

	for _, test := range testTable {
		result := stringToBinaryNum(test.line, test.zeroChar, test.oneChar)

		if result != test.expected {
			t.Errorf("Input %s, expected %d but got %d", test.line, test.expected, result)
		}
	}

}

func TestLineToRowColumn(t *testing.T) {
	var testTable = []struct {
		line           string
		expectedRow    int
		expectedColumn int
	}{
		{"BFFFBBFRRR", 70, 7},
		{"FFFBBBFRRR", 14, 7},
		{"BBFFBBFRLL", 102, 4},
	}

	for _, test := range testTable {
		resultRow, resultColumn := lineToRowColumn(test.line)

		if resultColumn != test.expectedColumn || resultRow != test.expectedRow {
			t.Errorf("Input %s, expected Row: %d, column: %d but got row: %d column: %d",
				test.line, test.expectedRow, test.expectedColumn, resultRow, resultColumn)
		}
	}
}
func TestGetSeatID(t *testing.T) {
	var testTable = []struct {
		row    int
		column int
		expectedID int
	}{
		{ 70, 7, 567},
		{ 14, 7, 119},
		{ 102, 4, 820},
	}

	for _, test := range testTable {
		resultID := getSeatID(test.row, test.column)

		if resultID != test.expectedID {
			t.Errorf("Input %d,%d, expected %d but got %d", test.row, test.column, test.expectedID, resultID)
		}

	}
}
