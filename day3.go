package main

import (
	"bufio"
	"os"
)

func main() {
	_, err := os.Stdin.Stat()
	if err != nil {
		panic(err)
	}

	input := getAllLines()

	answer := rideToboggan(input, 1, 1) *
		rideToboggan(input, 3, 1) *
		rideToboggan(input, 5 , 1) *
		rideToboggan(input, 7, 1) *
		rideToboggan(input, 1, 2)
	
	println(answer)
}

func rideToboggan(forest []string, right int, down int) int {
	xPos := 0
	hitTrees := 0
	for yPos, line := range forest {
		if yPos%down == 0 {
			runeLine := []rune(line)
			xIndex := xPos % len(runeLine)
			if runeLine[xIndex] == '#' {
				hitTrees++
			}
			xPos += right
		}
	}
	return hitTrees
}

func getAllLines() []string {
	scanner := bufio.NewScanner(os.Stdin)
	var output []string
	for scanner.Scan() {
		output = append(output, scanner.Text())
	}
	return output
}
