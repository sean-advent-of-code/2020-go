package main

import (
	"bufio"
	"os"
	"strings"
)

func main() {
	input := getAllLines()


	count := 0
	var stringBuilder string = ""
	newString := true
	for _, line := range input {
		if line == "" {
			count += len(stringBuilder)
			stringBuilder = ""
			newString = true
		} else {
			if newString {
				stringBuilder = line
				newString = false
			} else {
				removeChars := ""
				for _, char := range stringBuilder {
					inString := false
					for _, char2 := range line {
						if char == char2 {
							inString = true
						}
					}
					if !inString {
						removeChars += string(char);
					}
				}
				for _, tired := range removeChars{
						stringBuilder = strings.Replace(stringBuilder, string(tired), "", -1)
				}

			}
		}
	}

count += len(stringBuilder)

	println("count is ", count)

}

func getAllLines() []string {
	scanner := bufio.NewScanner(os.Stdin)
	var output []string
	for scanner.Scan() {
		output = append(output, scanner.Text())
	}
	return output
}

