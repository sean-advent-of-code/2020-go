package main

import (
	"bufio"
	"os"
	"regexp"
	"strconv"
	"strings"
)

func main() {
	_, err := os.Stdin.Stat()
	if err != nil {
		panic(err)
	}

	input := getAllLines()

	passports := parseDoubleNewLines(input, " ")
	validCount := 0

	for _, passport := range passports {
		if validatePassportMap(stringToMap(passport, ":", " ")) {
			validCount++
		}
	}

	println(validCount)
}

func validatePassportMap(passport map[string]string) bool {
	if byr, keyExist := passport["byr"]; !keyExist {
		return false
	} else {
		byrNum, _ := strconv.Atoi(byr)
		if byrNum < 1920 || byrNum > 2002 {
			return false
		}
	}
	if iyr, keyExist := passport["iyr"]; !keyExist {
		return false
	} else {
		iyrNum, _ := strconv.Atoi(iyr)
		if iyrNum < 2010 || iyrNum > 2020 {
			return false
		}
	}
	if eyr, keyExist := passport["eyr"]; !keyExist {
		return false
	} else {
		eyrNum, _ := strconv.Atoi(eyr)
		if eyrNum < 2020 || eyrNum > 2030 {
			return false
		}
	}
	if hgt, keyExist := passport["hgt"]; !keyExist {
		return false
	} else {
		runes := []rune(hgt)
		unit := runes[len(runes)-2:]
		value, _ := strconv.Atoi(string(runes[:len(runes)-2]))

		if string(unit) == "cm" {
			if value < 150 || value > 193 {
				return false
			}
		} else {
			if value < 59 || value > 76 {
				return false
			}
		}
	}
	if hcl, keyExist := passport["hcl"]; !keyExist {
		return false
	} else {
		matched, _ := regexp.MatchString(`^#[0-9a-f]{6}$`, hcl)
		if !matched {
			return false
		}

	}
	if ecl, keyExist := passport["ecl"]; !keyExist {
		return false
	} else {
		matched, _ := regexp.MatchString(`^(amb)|(blu)|(brn)|(gry)|(grn)|(hzl)|(oth)$`, ecl)
		if !matched {
			return false
		}

	}
	if pid, keyExist := passport["pid"]; !keyExist {
		return false
	} else {
		matched, _ := regexp.MatchString(`^\d{9}$`, pid)
		if !matched {
			return false
		}

	}
	return true
}

func getAllLines() []string {
	scanner := bufio.NewScanner(os.Stdin)
	var output []string
	for scanner.Scan() {
		output = append(output, scanner.Text())
	}
	return output
}

// the bellow function concatenates double line separated input to deliminator separated input
func parseDoubleNewLines(input []string, deliminator string) []string {
	var output []string
	var stringBuilder string = ""
	for _, line := range input {
		if line == "" {
			output = append(output, strings.TrimSpace(stringBuilder))
			stringBuilder = ""
		} else {
			stringBuilder = stringBuilder + deliminator + line
		}
	}
	output = append(output, strings.TrimSpace(stringBuilder))
	return output
}

func stringToMap(line string, keyValueSplitter string, deliminator string) map[string]string {
	if keyValueSplitter == deliminator {
		panic("this function wont work when the keyValueSplitter and the deliminator are the same")
	}

	var output map[string]string
	output = make(map[string]string)

	keyValues := strings.Split(line, deliminator)
	for _, keyValue := range keyValues {
		key1value2 := strings.Split(keyValue, keyValueSplitter)
		output[key1value2[0]] = key1value2[1]
	}

	return output

}
