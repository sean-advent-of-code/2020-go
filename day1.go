package main

import (
	"bufio"
	"os"
	"strconv"
)

func main() {
	_, err := os.Stdin.Stat()
	if err != nil {
		panic(err)
	}

	scanner := bufio.NewScanner(os.Stdin)
	var output []int

	for scanner.Scan() {
		val, _ := strconv.Atoi(scanner.Text())
		output = append(output, val)
	}
	for _, val := range output {
		for _, val2 := range output {
			for _, val3 := range output {

				if (val+val2+val3 == 2020) {
					println(val * val2 * val3)
				}
			}
		}
	}

}
