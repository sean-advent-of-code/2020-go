package main

import (
	"bufio"
	"os"
	"strconv"
	"strings"
)

func main() {
	_, err := os.Stdin.Stat()
	if err != nil {
		panic(err)
	}

	input := getAllLines()
	validPasswords := 0

	for _, val := range input {
		if validPassword(val) {
			validPasswords++
		}
	}

	println(validPasswords)

}

func validPassword(line string) bool {
	min, max, letter, password := lineToData(line)
	checkLetterMin := []rune(password)[min-1]
	checkLetterMax := []rune(password)[max-1]

	if checkLetterMax == letter || checkLetterMin == letter {
		if checkLetterMax == letter && checkLetterMin == letter {
			return false
		}
		return true
	}
	return false
}

func getAllLines() []string {
	scanner := bufio.NewScanner(os.Stdin)
	var output []string
	for scanner.Scan() {
		output = append(output, scanner.Text())
	}
	return output
}

func lineToData(line string) (int, int, rune, string) {
	split := strings.Split(line, " ")
	numbers := strings.Split(split[0], "-")

	min, _ := strconv.Atoi(numbers[0])
	max, _ := strconv.Atoi(numbers[1])
	letter := []rune(split[1])[0]

	return min, max, letter, split[2]

}
