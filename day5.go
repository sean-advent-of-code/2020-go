package main

import (
	"bufio"
	"fmt"
	"math"
	"os"
)

func main() {
	input := getAllLines()

	numberOfBits := 10
	maxPossibleSeatID := int(math.Pow(2, float64(numberOfBits)))
	sumOfAllSeatIDs := sumOfIntegers(maxPossibleSeatID)
	maxID := 0
	minID := maxPossibleSeatID
	for _, line := range input {
		currentID := getSeatID(lineToRowColumn(line))
		if currentID > maxID {
			maxID = currentID
		}

		if currentID < minID {
			minID = currentID
		}
		sumOfAllSeatIDs -= currentID
	}
	sumOfAllSeatIDs -= sumOfIntegers(minID-1)
	sumOfAllSeatIDs -= sumOfIntegers(maxPossibleSeatID) - sumOfIntegers(maxID) // because I am taking away from the total sum I don't need t add one to max id
	fmt.Printf("your seatID is %d\n", sumOfAllSeatIDs)

}

func sumOfIntegers(num int) int {
	return (num * (num + 1)) / 2
}

func getAllLines() []string {
	scanner := bufio.NewScanner(os.Stdin)
	var output []string
	for scanner.Scan() {
		output = append(output, scanner.Text())
	}
	return output
}

func stringToBinaryNum(line string, zeroChar rune, oneChar rune) int {
	runes := []rune(line)
	num := 0
	for _, char := range runes {
		if char == zeroChar || char == oneChar {
			num = num << 1
			if char == oneChar {
				num = num | 1
			}
		}
	}
	return num
}

func lineToRowColumn(line string) (int, int) {
	runes := []rune(line)
	rowString := string(runes[:7])
	columnString := string(runes[7:])

	row := stringToBinaryNum(rowString, 'F', 'B')
	column := stringToBinaryNum(columnString, 'L', 'R')

	return row, column
}

func getSeatID(row int, column int) int {
	return row*8 + column
}
